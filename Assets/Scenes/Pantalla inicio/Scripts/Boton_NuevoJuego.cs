﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boton_NuevoJuego : MonoBehaviour {
    private AudioSource[] allAudios;
	// Use this for initialization
	void Start () {
		
	}

    private void OnMouseDown() //Este metodo funciona cuando dan clic sobre un objeto
    {
        SceneManager.LoadScene("Intro");
        stopAllAudio();
    }

    void stopAllAudio()//Este metodo funciona para buscar y eliminar todos los sonidos de una escena
    {
        allAudios = FindObjectsOfType( typeof (AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudios)
        {
            audioS.Stop();
        }
    }
}
