﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManejadorMusica : MonoBehaviour {
    public static AudioSource[] allAudios;
    // Use this for initialization
    void Start () {
		
	}



    public static void stopAllAudio()
    {
        allAudios = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudios)
        {
            audioS.Stop();
        }
    }
}
