﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciarEscena : MonoBehaviour {
    GameObject timmy;
    GameObject nombre;
    // Use this for initialization
    void Start() {
        //GameObject.Find("Camina").SetActive(false);
        timmy = GameObject.Find("Camina");
        timmy.SetActive(false);
        nombre = GameObject.Find("Nombre");
        nombre.SetActive(false);
    }


    void MostrarTimmy()
    {
        if (ManejadorTexto.count == 6)
        {
            timmy.SetActive(true);
        }
    }

    private void Update()
    {
        MostrarNombre();
        MostrarTimmy();
    }

    void MostrarNombre()
    {
        if (ManejadorTexto.count == 2)
        {
            nombre.SetActive(true);
        }
    }
	
}
