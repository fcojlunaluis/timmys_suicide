﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogo_Intermedio_Garage : MonoBehaviour {

    public Dialogo dialogo;


    public void Trigger()
    {

            FindObjectOfType<ManejadorTexto_Intermedio_Garage>().iniciarDialogo(dialogo);


    }
}
