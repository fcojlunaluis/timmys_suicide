﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading;

public class ManejadorTexto_Intermedio_Patio : MonoBehaviour
{
    private Queue<string> oraciones;
    public Text nameTxt;
    public Text dialogotxx;
    bool first;
    public static int count;

    // Use this for initialization
    void Start()
    {
        oraciones = new Queue<string>();
        first = true;
        count = 0;
        if (nameTxt.text == "Dios")
        {
            GameObject.Find("Nombre").SetActive(false);

        }

    }

    public void iniciarDialogo(Dialogo dialogo)
    {

        if (first)
        {
            nameTxt.text = dialogo.nombre + ":";
            oraciones.Clear();
            foreach (string oracion in dialogo.oraciones)
            {
                oraciones.Enqueue(oracion);
            }
            first = false;

        }
        else
        {
            mostrarSiguienteOracion();
        }

    }

    IEnumerator TypeSentece(string oracion)
    {
        dialogotxx.text = "";
        foreach (char letra in oracion.ToCharArray())
        {
            dialogotxx.text += letra;
            yield return null;
        }
    }

    public void mostrarSiguienteOracion()
    {

        if (oraciones.Count == 0)
        {
            finDialogo();
        }
        string oracion = oraciones.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentece(oracion));
        count++;

    }

    bool finDialogo()
    {
        ManejadorMusica.stopAllAudio();
        SceneManager.LoadScene("Cuarto_Timmy_Patio");
        return true;
    }
}
