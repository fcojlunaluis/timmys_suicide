﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogo_Intermedio_Patio : MonoBehaviour {

    public Dialogo dialogo;


    public void Trigger()
    {

            FindObjectOfType<ManejadorTexto_Intermedio_Patio>().iniciarDialogo(dialogo);


    }
}
