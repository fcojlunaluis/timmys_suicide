﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accion_Gatage : MonoBehaviour {


    public GameObject timmy;

    Animator ani;

    public void BotonSalvar()
    {
        ani = timmy.GetComponent<Animator>();
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("fosforo_cayendo"))
        {
            ani.SetTrigger("salvar");
        }
    }
}