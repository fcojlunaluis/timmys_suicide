﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desactivar_objeto : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Puerta_Salida")
        {
            this.gameObject.SetActive(false);
        }
    }

}
