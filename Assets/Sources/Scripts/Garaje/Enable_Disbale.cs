﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enable_Disbale : MonoBehaviour {


    public GameObject boton;
    public GameObject texto;
    public GameObject timmy;
    public GameObject gasolina;

    Animator ani;

    // Use this for initialization
    void Start()
    {
        boton.transform.position = GameObject.Find("BtnPos").transform.position;
        GameObject.Find("Cuadro_Dialogo").transform.position = GameObject.Find("cuadroPos").transform.position;
        ani = timmy.GetComponent<Animator>();
        boton.gameObject.SetActive(false);
        texto.gameObject.SetActive(false);
        gasolina.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("fosforo_cayendo"))
        {

            boton.gameObject.SetActive(true);
            texto.gameObject.SetActive(true);
        }

    }
}
