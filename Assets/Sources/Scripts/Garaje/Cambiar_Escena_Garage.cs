﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cambiar_Escena_Garage : MonoBehaviour {
    public Animator ani;
    bool des;
	// Use this for initialization
	void Start () {
        des = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (ani.GetCurrentAnimatorStateInfo(ani.GetLayerIndex("BaseLayer")).IsName("Depresed"))
        {
            ManejadorMusica.stopAllAudio();
            SceneManager.LoadScene("Intermedio_Garage");
        }else if (ani.GetCurrentAnimatorStateInfo(ani.GetLayerIndex("BaseLayer")).IsName("Tirar_Gasolina") && des)
        {
            GameObject.Find("Cuadro_Dialogo").SetActive(false);
            des = false;
        }

    }
}
