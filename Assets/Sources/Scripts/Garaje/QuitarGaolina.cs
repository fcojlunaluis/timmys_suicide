﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitarGaolina : MonoBehaviour {

    Animator ani;
    public GameObject timmy;
    public GameObject gasolina;

    // Use this for initialization
    void Start () {
        ani = timmy.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("tomar_gasolina"))
        {
            gasolina.SetActive(false);            
        }
    }
}
