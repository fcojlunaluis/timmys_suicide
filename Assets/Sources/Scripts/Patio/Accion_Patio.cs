﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accion_Patio : MonoBehaviour {

    public GameObject timmy;

    Animator ani;

    public void BotonSalvar()
    {
        ani = timmy.GetComponent<Animator>();
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("Ahogo"))
        {
            ani.SetTrigger("salvar");
        }
    }
}

