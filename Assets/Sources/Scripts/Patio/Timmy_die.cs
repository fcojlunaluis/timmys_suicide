﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Timmy_die : MonoBehaviour {
    float tiempoEspera;
    float tiempo;
    Animator ani;
	// Use this for initialization
	void Start () {
        tiempoEspera = 1000000f;
        tiempo = 0;
        ani = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        tiempo += 0.1f;
        if ( !(tiempoEspera > tiempo)  && ani.GetAnimatorTransitionInfo(ani.GetLayerIndex("BaseLayer")).IsName("Ahogo") )
        {
            TimmyDie();
        }
	}

    void TimmyDie()
    {
        Thread.Sleep(4000);
        ani.SetTrigger("die");
    }
}
