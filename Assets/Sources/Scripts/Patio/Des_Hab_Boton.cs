﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Des_Hab_Boton : MonoBehaviour {

    public GameObject boton;
    public GameObject texto;
    public GameObject timmy;

    Animator ani;

    // Use this for initialization
    void Start() {
        boton.transform.position = GameObject.Find("BtnPos").transform.position;
        GameObject.Find("Cuadro_Dialogo").transform.position = GameObject.Find("cuadro_Pos").transform.position;
        ani = timmy.GetComponent<Animator>();
        boton.gameObject.SetActive(false);
        texto.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("Ahogo"))
        {

            boton.gameObject.SetActive(true);
            texto.gameObject.SetActive(true);
        }

    }
}
