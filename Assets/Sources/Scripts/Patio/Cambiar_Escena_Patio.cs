﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cambiar_Escena_Patio : MonoBehaviour {
    public Animator ani;
    public static bool des;
    public static bool die;
	// Use this for initialization
	void Start () {
        des = true;
        die = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (ani.GetCurrentAnimatorStateInfo(ani.GetLayerIndex("BaseLayer")).IsName("Parado_front"))
        {
            ManejadorMusica.stopAllAudio();
            SceneManager.LoadScene("Intermedio_Patio");
        }else if (des  && ani.GetCurrentAnimatorStateInfo(ani.GetLayerIndex("BaseLayer")).IsName("Ahogo"))
        {
            GameObject.Find("Cuadro_Dialogo").SetActive(false);
            des = false;
            die = true;
        }
        else if (die  && ani.GetCurrentAnimatorStateInfo(ani.GetLayerIndex("BaseLayer")).IsName("die"))
        {
            ManejadorMusica.stopAllAudio();
            SceneManager.LoadScene("Intermedio_Patio");
        }

    }
}
